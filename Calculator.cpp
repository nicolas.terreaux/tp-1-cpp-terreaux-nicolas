#include <cmath>
#include "Calculator.hpp"

Calculator::Calculator() {}

//Try to convert the string to a double and return true if it worked
bool Calculator::check_double(std::string arg) {
    try {
        std::stod(arg);
        return 1;
    }
    catch (std::exception &e) {
        return 0;
    }
}

//Check if the given operator exists
char Calculator::check_operator(std::string arg) {
    if (arg.size() != 1) {
        return ERR;
    }
    switch (arg[0]) {
        case '+':
            return ADD;
        case '-':
            return SUB;
        case '*':
            return MUL;
        case '/':
            return DIV;
        case '#':
            return END;
        case '%':
            return MOD;
        default:
            return ERR;
    }
}

//Executes the calculation with the given parameters
double Calculator::calculate(std::string firstNumber, char oper, std::string secondNumber) {
    double number1 = std::stod(firstNumber);
    double number2 = std::stod(secondNumber);

    switch (oper) {
        case ADD:
            return number1 + number2;
        case MUL:
            return number1 * number2;
        case DIV:
            return number1 / number2;
        case SUB:
            return number1 - number2;
        case MOD:
            return std::fmod(number1, number2);
        default:
            return 0;
    }
}

