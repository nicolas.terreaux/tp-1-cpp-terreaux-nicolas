#include <iostream>
#include "Calculator.hpp"

int main() {
    std::cout << "---Start calculator---" << std::endl;
    std::string firstNumber;
    std::string secondNumber;
    std::string oper;
    char charOper;
    double sum;
    bool doubleOK = false;
    bool operatorOk = false;

    //Creation of the pointer and reference
    Calculator calc = Calculator();
    Calculator *calculator = &calc;

    //First time asking for a number
    std::cout << "Give your first number" << std::endl;
    while (!doubleOK) {
        std::cin >> firstNumber;
        if (!calc.check_double(firstNumber)) {
            std::cout << "Not a double please enter a valid number for the first number" << std::endl;
        } else {
            doubleOK = true;
        }
    }

    //Loop until the user gives a # as operator
    while (true) {
        doubleOK = false;
        std::cout << "Give the operator" << std::endl;
        while (!operatorOk) {
            std::cin >> oper;
            charOper = calculator->check_operator(oper);
            if (charOper == ERR) {
                std::cout << "Not a valid operator" << std::endl;
            } else {
                operatorOk = true;
                if (charOper == END) return 0;
            }
        }
        operatorOk = false;
        std::cout << "Give your second number" << std::endl;
        while (!doubleOK) {
            std::cin >> secondNumber;
            if (!calculator->check_double(secondNumber)) {
                std::cout << "Not a double please enter a valid number for the second" << std::endl;
            } else if ((charOper == DIV) && (std::stod(secondNumber) == 0)) {
                std::cout << "Cannot divide by 0" << std::endl;
                break;
            } else {
                doubleOK = true;
            }
        }
        if (doubleOK) {

            //Calculate the sum
            sum = calculator->calculate(firstNumber, charOper, secondNumber);

            //Displays the final calculation
            std::cout << std::stod(firstNumber) << " " << charOper << " " << secondNumber << " = "
                      << sum << std::endl;

            //Replace the first number with the sum of the calculation
            firstNumber = std::to_string(sum);
        }
    }
}
