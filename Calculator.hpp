// Calculator.h

#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <iostream>

enum Oper {
    MUL = '*',
    ADD = '+',
    SUB = '-',
    DIV = '/',
    MOD = '%',
    END = '#',
    ERR = '?'
};

class Calculator {

public:
    Calculator();

    bool check_double(std::string arg);

    char check_operator(std::string arg);

    double calculate(std::string firstNumber, char oper, std::string secondNumber);
};

#endif /* CALCULATOR_H */
